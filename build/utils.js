'use strict'
const path = require('path')
const config = require('../config')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const packageConfig = require('../package.json')

const glob = require('glob-all')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// 获取多个入口
exports.getEntries = (dirPaths) => {
  let filePaths = glob.sync(dirPaths);
  let entries = {};
  filePaths.forEach(filePath => {
    let filename = filePath.match(/([\w_-]+)\.js$/)[1];
    entries[filename] = filePath;
  })
  return entries;
}

const getHtml = (dirPaths) => {
  let filePaths = glob.sync(dirPaths);
  let htmls = {};
  filePaths.forEach(filePath => {
    let filename = filePath.match(/([\w_-]+)\.html$/)[1];
    htmls[filename] = filePath;
  })
  return htmls;
}

// 创建多个HtmlWebpackPlugin
exports.createHtmlWebpackPlugins = () =>  {
  const htmls = getHtml(path.join(__dirname, '..', 'src/pages/*/*.html'))
  let plugins = []
  if (process.env.NODE_ENV === 'production') {
    for (const pageName in htmls) {
      const path = htmls[pageName]
      plugins.push(new HtmlWebpackPlugin({
        filename: pageName + '.html',
        template: path,
        inject: true,
        minify: {
          removeComments: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true
          // more options:
          // https://github.com/kangax/html-minifier#options-quick-reference
        },
        // necessary to consistently work with multiple chunks via CommonsChunkPlugin
        chunksSortMode: 'dependency',
        chunks: ['manifest', 'vendor', pageName]
      }))
    }
  }else {
    for (const pageName in htmls) {
      const path = htmls[pageName]
      plugins.push(new HtmlWebpackPlugin({
        filename: pageName + '.html',
        template: path,
        inject: true,
        chunks: ['manifest', 'vendor', pageName]
      }))
    }
  }
  return plugins
}

exports.assetsPath = function (_path) {
  const assetsSubDirectory = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectory
    : config.dev.assetsSubDirectory

  return path.posix.join(assetsSubDirectory, _path)
}

exports.cssLoaders = function (options) {
  options = options || {}

  const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap: options.sourceMap
    }
  }

  const postcssLoader = {
    loader: 'postcss-loader',
    options: {
      sourceMap: options.sourceMap
    }
  }

  // generate loader string to be used with extract text plugin
  function generateLoaders (loader, loaderOptions) {
    const loaders = options.usePostCSS ? [cssLoader, postcssLoader] : [cssLoader]

    if (loader) {
      loaders.push({
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      })
    }

    // Extract CSS when that option is specified
    // (which is the case during production build)
    if (options.extract) {
      return ExtractTextPlugin.extract({
        use: loaders,
        fallback: 'vue-style-loader'
      })
    } else {
      return ['vue-style-loader'].concat(loaders)
    }
  }

  // https://vue-loader.vuejs.org/en/configurations/extract-css.html
  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    less: generateLoaders('less'),
    sass: generateLoaders('sass', { indentedSyntax: true }),
    scss: generateLoaders('sass'),
    stylus: generateLoaders('stylus'),
    styl: generateLoaders('stylus')
  }
}

// Generate loaders for standalone style files (outside of .vue)
exports.styleLoaders = function (options) {
  const output = []
  const loaders = exports.cssLoaders(options)

  for (const extension in loaders) {
    const loader = loaders[extension]
    output.push({
      test: new RegExp('\\.' + extension + '$'),
      use: loader
    })
  }

  return output
}

exports.createNotifierCallback = () => {
  const notifier = require('node-notifier')

  return (severity, errors) => {
    if (severity !== 'error') return

    const error = errors[0]
    const filename = error.file && error.file.split('!').pop()

    notifier.notify({
      title: packageConfig.name,
      message: severity + ': ' + error.name,
      subtitle: filename || '',
      icon: path.join(__dirname, 'logo.png')
    })
  }
}
