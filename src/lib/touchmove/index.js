import $ from 'jquery'

export default (selector, {
  offset = 50,
  left = () => {},
  right = () => {},
  top = () => {},
  bottom = () => {},
  touch = () => {}
}) => {
  let startX
  let startY
  let endMove = true
  $(selector).on('touchstart', (e) => {
    e.preventDefault()
    startX = e.changedTouches[0].pageX
    startY = e.changedTouches[0].pageY
    endMove = false
  })

  $(selector).on('touchmove', (e) => {
    e.preventDefault()
    if (endMove === true) return
    const moveEndX = e.changedTouches[0].pageX
    const moveEndY = e.changedTouches[0].pageY
    const X = moveEndX - startX
    const Y = moveEndY - startY

    if (Math.abs(X) > Math.abs(Y) && X > offset) {
      right()
      endMove = true
    } else if (Math.abs(X) > Math.abs(Y) && X < -offset) {
      left()
      endMove = true
    } else if (Math.abs(Y) > Math.abs(X) && Y > offset) {
      bottom()
      endMove = true
    } else if (Math.abs(Y) > Math.abs(X) && Y < -offset) {
      top()
      endMove = true
    }
  })

  $(selector).on('touchend', (e) => {
    if (endMove === true) return
    touch()
    endMove = true
  })
}
