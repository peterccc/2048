import Vue from 'vue'
import Item from '../Item'
import $ from 'jquery'

const ItemClass = Vue.extend(Item)

export default class extends ItemClass {
  constructor (selector, value, x, y) {
    super({
      propsData: {
        value: value,
        x: x,
        y: y
      }
    })
    this.$mount()
    $(selector).append(this.$el)
  }
  remove () {
    this.show.remove()
  }
}
