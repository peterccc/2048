import Flag from './Flag'
import Item from './Item'

export default class GameItem {
  item
  addFlag
  constructor (x, y, value, selector) {
    this.item = new Item(selector, value, x, y)
    this.addFlag = new Flag()
  }

  remove () {
    this.item.remove()
  }

  setCoordinate (x, y) {
    this.item.x = x
    this.item.y = y
  }

  setValue (value) {
    this.item.value = value
  }

  getCoordinate () {
    return {
      x: this.item.x,
      y: this.item.y
    }
  }

  getValue () {
    return this.item.value
  }

  setAddFlag () {
    this.addFlag.set()
  }

  initAddFlag () {
    this.addFlag.init()
  }

  checkAddFlag () {
    return this.addFlag.check()
  }
}
