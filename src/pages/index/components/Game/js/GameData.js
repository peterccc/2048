import GameItem from './GameItem'
import Flag from './Flag'
import _ from 'lodash'

export const direction = {
  top: 38,
  right: 39,
  bottom: 40,
  left: 37
}
export default class GameData {
  constructor (boardSelector, width, height) {
    this.boardSelector = boardSelector
    this.width = width
    this.height = height
    // 全部填空
    this.gameData = new Array(this.width)
    for (let x = 0; x < this.width; ++x) {
      this.gameData[x] = new Array(this.height)
      for (let y = 0; y < this.height; ++y) {
        this.gameData[x][y] = null
      }
    }

    // 添加第一个
    this.appendItem()

    // 初始化标志
    this.moveFlag = new Flag()
  }
  getDataTestMap () {
    let data = new Array(this.height)
    for (let y = 0; y < this.height; ++y) {
      data[y] = new Array(this.width)
      for (let x = 0; x < this.width; ++x) {
        data[y][x] = this.gameData[x][y]
      }
    }
    return data
  }
  getDataMap () {
    return this.gameData
  }
  getDataList () {
    let x = this.width
    let y = this.height
    let list = []
    while (x--) {
      while (y--) {
        const gameItem = this.gameData[x][y]
        if (gameItem) {
          list.push(this.gameData[x][y])
        }
      }
      y = this.height
    }
    return list
  }
  // 获取没有item的位置
  getEmptyList () {
    let emptyList = []
    for (let x = 0; x < this.width; ++x) {
      for (let y = 0; y < this.height; ++y) {
        if (this.gameData[x][y] === null) {
          emptyList.push({x, y})
        }
      }
    }
    return emptyList
  }
  // 随机添加一个item
  appendItem () {
    const emptyList = this.getEmptyList()
    if (emptyList.length === 0) return // TODO: GameOver
    const index = _.random(0, emptyList.length - 1)
    let value = null
    if (_.random(0, 1) === 0) {
      value = 2
    } else {
      value = 4
    }
    const coordinate = emptyList[index]
    const gameItem = new GameItem(coordinate.x, coordinate.y, value, this.boardSelector)
    this.gameData[coordinate.x][coordinate.y] = gameItem
  }
  validRange (x, y) {
    return y >= 0 && y < this.height && x >= 0 && x < this.width
  }
  getItem (x, y) {
    if (!this.validRange(x, y)) return null
    return this.gameData[x][y]
  }
  getItemMovePosition (item, next, previous) {
    let c = item.getCoordinate()
    // 移动一格
    c = next(c)
    // 看看是否越界
    while (this.validRange(c.x, c.y)) {
      // 看看是否存在Item
      const distItem = this.gameData[c.x][c.y]
      if (distItem) {
        // 看看目标Item数值是否与目前Item相同
        if (distItem.getValue() !== item.getValue()) {
          // 不相同则返回上一次位置
          c = previous(c)
          // 相同则返回目标位置
        }
        return c
      }
      // 不存在或未到边界则下一格
      c = next(c)
    }
    // 越过边界退回上一格
    c = previous(c)
    // 返回边界位置
    return c
  }
  getItemMoveTopPosition (item) {
    return this.getItemMovePosition(item, c => {
      --c.y; return c
    }, c => {
      ++c.y; return c
    })
  }
  getItemMoveRightPosition (item) {
    return this.getItemMovePosition(item, c => {
      ++c.x; return c
    }, c => {
      --c.x; return c
    })
  }

  getItemMoveBottomPosition (item) {
    return this.getItemMovePosition(item, c => {
      ++c.y; return c
    }, c => {
      --c.y; return c
    })
  }
  getItemMoveLeftPosition (item) {
    return this.getItemMovePosition(item, c => {
      --c.x; return c
    }, c => {
      ++c.x; return c
    })
  }
  move (item, distPos) {
    const srcPos = item.getCoordinate()
    // 目标Item
    let distItem = this.gameData[distPos.x][distPos.y]
    // 判断是否相加过
    if (item.checkAddFlag()) {
      return
    }
    if (distItem && distItem.checkAddFlag()) {
      return
    }
    // 判断是否相加
    if (distItem && distItem !== item) {
      // 存在则相加
      item.setValue(distItem.getValue() + item.getValue())
      item.setAddFlag()
      distItem.remove()
    }
    // 如果以前位置跟现在位置不相等，则清除以前Item的位置
    if (srcPos.x !== distPos.x || srcPos.y !== distPos.y) {
      this.gameData[srcPos.x][srcPos.y] = null
      // 覆盖或移动
      this.gameData[distPos.x][distPos.y] = item
      // 更新item信息
      item.setCoordinate(distPos.x, distPos.y)
      // 设置移动标志
      this.moveFlag.set()
    }
  }
  moveTop () {
    for (let y = 1; y < this.height; ++y) {
      for (let x = 0; x < this.width; ++x) {
        // 目前Item位置
        let item = this.gameData[x][y]
        // 没有Item则下一个
        if (!item) continue
        // 目标位置
        let distPos = this.getItemMoveTopPosition(item)
        // 移动
        this.move(item, distPos)
      }
    }
  }
  moveRight () {
    for (let x = this.width - 2; x >= 0; --x) {
      for (let y = 0; y < this.height; ++y) {
        // 目前Item位置
        let item = this.gameData[x][y]
        // 没有Item则下一个
        if (!item) continue
        // 目标位置
        let distPos = this.getItemMoveRightPosition(item)
        // 移动
        this.move(item, distPos)
      }
    }
  }
  moveBottom () {
    for (let y = this.height - 2; y >= 0; --y) {
      for (let x = 0; x < this.width; ++x) {
        // 目前Item位置
        let item = this.gameData[x][y]
        // 没有Item则下一个
        if (!item) continue
        // 目标位置
        let distPos = this.getItemMoveBottomPosition(item)
        // 移动
        this.move(item, distPos)
      }
    }
  }
  moveLeft () {
    for (let x = 1; x < this.width; ++x) {
      for (let y = 0; y < this.height; ++y) {
        // 目前Item位置
        let item = this.gameData[x][y]
        // 没有Item则下一个
        if (!item) continue
        // 目标位置
        let distPos = this.getItemMoveLeftPosition(item)
        // 移动
        this.move(item, distPos)
      }
    }
  }

  // 用户操作
  operator (keyCode) {
    // 初始化 flag
    this.moveFlag.init()
    this.getDataList().forEach(item => {
      item.initAddFlag()
    })

    // 向上
    if (keyCode === direction.top) {
      this.moveTop()
    } else if (keyCode === direction.bottom) {
      this.moveBottom()
    } else if (keyCode === direction.left) {
      this.moveLeft()
    } else if (keyCode === direction.right) {
      this.moveRight()
    }

    // 检查是否移动，移动后添加一个
    if (this.moveFlag.check()) {
      if (keyCode === direction.top ||
        keyCode === direction.right ||
        keyCode === direction.bottom ||
        keyCode === direction.left
      ) {
        this.appendItem()
      }
    }
  }
}
