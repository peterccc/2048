export default class Flag {
  constructor () {
    this.flag = false
  }
  init () {
    this.flag = false
  }
  set () {
    this.flag = true
  }
  check () {
    return this.flag
  }
}
