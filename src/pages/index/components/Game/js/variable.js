let variable = {
  itemXCount: 4,
  itemYCount: 4,
  itemPadding: 0.04,
  itemWidth: 0.65,
  itemHeight: 0.65
}
variable.boardWidth = variable.itemXCount * variable.itemPadding * 2 + variable.itemXCount * variable.itemWidth
variable.boardHeight = variable.itemYCount * variable.itemPadding * 2 + variable.itemYCount * variable.itemHeight
export default variable
